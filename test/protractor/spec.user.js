// spec.js
describe('User App', function() {
  var newUserPageBtn = element(by.linkText('New User'));
  var newUserBtn = element(by.buttonText('Create User'));
  var editUserBtn  = element(by.buttonText('Edit'));
  var updateUserBtn = element(by.buttonText('Update User'));
  var users = element.all(by.css('table tbody tr'));
  var body = element(by.css('body'));

  it('should create a new user', function() {
    browser.get('http://54.169.123.170:32805/');

    newUserPageBtn.click();

    element(by.id('user_firstname')).sendKeys('Bernie');
    element(by.id('user_lastname')).sendKeys('Chiu');
    element(by.id('user_age')).sendKeys('26');
    element(by.cssContainingText('option', 'others')).click();
    element(by.id('user_address_county')).sendKeys('Taoyuan');
    element(by.id('user_address_address_1')).sendKeys('Taichang Street');
    element(by.id('user_address_address_2')).sendKeys('Zhongping Street');

    newUserBtn.click();

    expect(body.getText()).toContain('Bernie');
    expect(body.getText()).toContain('Chiu');
    expect(body.getText()).toContain('26');
    expect(body.getText()).toContain('others');
    expect(body.getText()).toContain('Taoyuan');
    expect(body.getText()).toContain('Taichang Street');
    expect(body.getText()).toContain('Zhongping Street');
  });

  it('should be able to update a user', function() {
    browser.get('http://54.169.123.170:32805/');
    var firstUser = users.first();

    firstUser.element(by.linkText('Edit')).click();
    firstUser.element(by.model('user.firstname')).clear().sendKeys('Peter');
    firstUser.element(by.buttonText('Update User')).click();
    expect(body.getText()).toContain('Peter');
  });

  it('should be able to destroy user', function() {
    browser.get('http://54.169.123.170:32805/');
    var firstUser = users.first();
    var userCount = users.count();

    userCount.then(function(count) {
      firstUser.element(by.linkText('Destroy')).click();
      browser.switchTo().alert().accept();
      expect(users.count()).toEqual(count - 1);
    });
  });
});
