class User
  GENDERS = ['male', 'female', 'others']

  include Mongoid::Document
  include Mongoid::Timestamps
  field :firstname, type: String
  field :lastname, type: String
  field :age, type: Integer
  field :gender, type: String
  field :address, type: Hash, default: {}

  has_one :shop, dependent: :destroy

  validates :firstname, presence: true
  validates :lastname,  presence: true
  validates :age, format: { with: /\A[1-9]\d*\z/i }
  validates :gender, inclusion: User::GENDERS
end
