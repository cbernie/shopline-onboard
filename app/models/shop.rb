class Shop
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String

  belongs_to :shop

  index shop_id: 1
end
