app.controller('userNavigationController', ['$scope', function($scope) {
  $scope.showUserForm = false;
  $scope.showForm = function() {
    $scope.showUserForm = !$scope.showUserForm;
  };
}]);
