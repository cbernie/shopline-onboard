app.controller(
'UsersEditController', [
  '$scope',
  '$http',
  '$window',
  'userService',
function($scope, $http, $window, userService) {
  var self = this;

  $scope.positiveNumbers = /^[1-9]\d*$/;
  $scope.userId = $scope.uid // Form directive attribute;

  userService.getUser($scope.userId).then(
    // On success
    function(data) {
      $scope.user = data;
    },
    // On failure
    function(data) {
      $scope.user = {};
    }
  );

  $scope.updateUser = function() {
    userService.updateUser($scope.user).then(
      // On success
      function(data) {
        $window.location.href = '/users/' + $scope.userId;
      },
      // On failure
      function(data) {
        return;
      }
    );
  };
}]).directive('userForm', function() {
  return {
    restrict: 'E',
    templateUrl: '/users/form.html',
    scope: { uid: '=' }
  };
});
