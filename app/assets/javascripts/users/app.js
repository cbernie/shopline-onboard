var app = angular.module('user_app', []);

app.config(["$httpProvider", function($httpProvider) {
  //need to comment out the below 2
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
  //Enable cross domain calls
  $httpProvider.defaults.useXDomain = true;
  //Remove the header used to identify ajax call  that would prevent CORS from working
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

}]);
