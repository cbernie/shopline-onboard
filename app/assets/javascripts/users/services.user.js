app.service('userService', ['$http', '$q', function($http, $q) {
  return {
    getUser: function(userId) {
      var promise = $http({
        method: 'GET',
        url: '/users/' + userId,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      });
      var deferredObject = $q.defer();

      promise.then(
        function(response) {
          deferredObject.resolve(response.data);
        },
        function(response) {
          deferredObject.reject(response);
        }
      );

      return deferredObject.promise;
    },
    updateUser: function(user) {
      var promise = $http({
        method: 'PUT',
        url: '/users/' + user.id.$oid,
        data: {
          user: user
        },
        headers: {
          'Accept': 'application/json'
        }
      });

      var deferredObject = $q.defer();

      promise.then(
        function(response) {
          deferredObject.resolve(response.data);
        },
        function(response) {
          deferredObject.reject(response);
        }
      );

      return deferredObject.promise;
    }
  };
}]);
