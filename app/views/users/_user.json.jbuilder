json.extract! user, :id, :firstname, :lastname, :age, :gender, :address, :created_at, :updated_at
json.url user_url(user, format: :json)
